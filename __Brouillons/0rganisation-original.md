# Organisation opérationnelle

Dans un MOOC, en général, se rencontrent deux publics :

- l'équipe pédagogique qui propose le contenu du MOOC (le cours et les exercices),
- les apprenants qui suivent le MOOC. 

Ce MOOC est un peu différent :

- l'équipe pédagogique est plus une équipe d'architectes qui propose un squelette d'organisation mais n'intervient pas pour dispenser des cours dans le MOOC ;
- les personnes apprenantes (vous … et nous) sommes à la fois celles et ceux qui suivent le MOOC, et qui en font évoluer le contenu (par les ressources produites, artefacts des activités pour apprendre à produire une ressource d'enseignement)
- il y a un troisième public (invisible dans le MOOC) : les élèves des classes (réelles ou imaginées) à qui vous destinez les activités que vous préparez.

Dans la suite, le terme _élèves_ désignent bien des élèves de lycée à qui les _fiches élèves_ sont destinées. Le terme _prof_ désigne **vous** (et nous), enseignantes ou enseignants, en situation d'apprenants dans ce MOOC. Nous mêmes enseignons en informatique au niveau de l'enseignement secondaire ou universitaire.

Le MOOC est fondé sur la production de ressources type _fiches_ qui vont venir expliciter le pourquoi et le comment d'activités _élèves_.

Ces fiches sont réalisées au format texte _markdown_ (_md_) et déposées dans un gitlab dédié (nous reviendrons sur son utilisation) : elles pourront faire l'objet d'évaluations par les pairs (via la plateforme FUN) après avoir été discutées sur le [forum NSI-SNT](https://mooc-forums.inria.fr/moocnsi/c/enseigner-linformatique/se-former-a-la-nsi-snt/136).


## Quelques exemples décortiqués

Pour comprendre la démarche par l'exemple prenons trois personnes qui suivraient ce MOOC (en fait ce sont des personnes réelles qui ont aidé à le préparer)

### Une activité de simulation d'Internet 

David Roche propose en SNT en classe de 2nde une séquence sur machines pour simuler Internet via l'outil [Filius](https://www.lernsoftware-filius.de/Herunterladen). Il dispose déjà d'une fiche élève et va dans le cadre de ce MOOC, s'entraîner à produire les éléments pédagogiques complémentaires.

- Sa fiche _élève_ est disponible sur un site web et la _fiche prof_ contiendra un lien vers la ressource 
- Pour répondre à l'exercice Niveau 1 du Module 1 : "Penser-Concevoir-Elaborer", David dépose [sa fiche _prof_](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_droche.md) au format _md_ sur dépôt gitlab (nous reviendrons plus loin sur l'utilisation de Markdown et du git avec des tutoriels) et peut transmettre le lien vers ce travail, sur la plateforme et sur le forum pour discussion et éventuellement correction par les paires s'il s'agit d'une activité notée de cette façon ;
- Dans le cadre du Niveau 1 du Module 2 : "Mettre-en-oeuvre-Animer", David décide de continuer avec son activité SNT de simulation d'internet et propose une [_analyse de l'activité_](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/fiche_deroule_droche.md), toujours au format _md_ et déposé au bon endroit dans son dépôt gitlab.

### Une activité de découverte du langage Python

Maxime Fourny propose en NSI en classe de 1re une séquence sur machine pour découvrir les premières instructions du langage Python à l'aide du module `turtle`. Après avoir testé son activité auprès des élèves, il la systématise sous forme de fiche élève. Sa fiche élève est un _pdf_ et il n'a pas d'espace pour la déposer. Il se sert alors du dossier de ses fiches _prof_ et _d'analyse de l'activité_ pour mettre sa ressource élève à disposition, dans le même espace, et réalise les autres fiches dans le cadre de ce MOOC.

- Sa fiche _élève_ : [fiche-eleve-nsi-1ere-decouverte-python.pdf](../Ressources/Fiche_001/fiche-eleve-nsi-1ere-decouverte-python.pdf) 
- Sa fiche _prof_ : [fiche-prof-nsi-1ere-decouverte-python.md](../Ressources/Fiche_001/fiche-prof-nsi-1ere-decouverte-python.md)
- Sa fiche _d'analyse de l'activité_ : [deroule-nsi-1ere-decouverte-python.md](../Ressources/Fiche_001/deroule-nsi-1ere-decouverte-python.md)


### Un activité hybride pour manipuler un parcours de graphe

Thierry Viéville n'est pas enseignant, mais suis ce MOOC et propose en NSI en classe de Terminale une activité débranchée et sur machines pour manipuler et réviser le vocabulaire des graphes et la recherche d'un plus court chemin, pour se former aussi à enseigner. Il formalise ce qu'il souhaite réaliser et se base sur les exemples et les formats fournis pour que la proposition puisse être partagée, évaluée et suivie.

- Sa fiche _élève_ : aucune car l'activité se découvre au _fil de l'eau_ 
- Sa fiche _prof_ : [fiche-prof-nsi-term-graphe-algo.md](../Ressources/Fiche_002/fiche-prof-nsi-term-graphe-algo.md)
- Sa fiche _d'analyse de l'activité_ : [deroule-nsi-term-graphe-algo.md](../Ressources/Fiche_002/deroule-nsi-term-graphe-algo.md)

## Les productions attendues

Voici maintenant concrètement ce qui est proposé de faire pour se former ensemble.

### Dans le module `1_Penser-Concevoir-Elaborer` 

Vous devez :

- Pour le niveau 1, récupérer un cours ou une activité _élève_ pour un niveau (SNT ou NSI), par exemple en allant sur https://pixees.fr/informatiquelycee/, ou alors créer votre propre activité (niveau 2)
- Récupérer le modèle de la [fiche _prof_](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/Ressources/00_fiche-prof-modele.md)
- Compléter cette fiche et la déposer sur votre espace gitlab, c'est à dire dans le répertoire `1_Penser-Concevoir-Elaborer` de l'arborescence que vous avez créé en faisant un "fork" du squelette proposé, comme [expliqué à la mise en place] (LIEN VERS TUTO)

### Dans le module `2_Mettre-en-oeuvre` 

Vous devez, en plus de la fiche _prof_ proposer une fiche _d'analyse de l'activité_. Les modalités techniques restant les mêmes.


### Dans le module `3_Accompagner`

_TODO_

### Dans le module `4_Observer-Analyser-Evaluer`

_TODO_

## Mais comment déposer mon travail ?

Les explications détaillées se trouvent à la fois dans :

- [un bref tutoriel du langage _Markdown_](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/0_Introduction/2_0rganisation_et_tutoriels/Tutoriels/1_tuto_markdown.md)
- [un tutoriel pour utiliser l'outil _Gitlab_](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/blob/master/0_Introduction/2_0rganisation_et_tutoriels/Tutoriels/2_tuto_gitlab.md)


## Droits et conditions d'utilisation

_TODO_ : Expliquer que les ressources seront sous une des [Licences _Creative Commons_](https://fr.wikipedia.org/wiki/Licence_Creative_Commons) CC-BY-NC-SA par exemple.
