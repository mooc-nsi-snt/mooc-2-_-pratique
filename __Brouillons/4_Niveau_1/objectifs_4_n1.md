## 4. Observer - Analyser - Évaluer
### Niveau 1

- Proposer un ensemble d'évaluations (courte en classe, devoir maison etc.) sur une ou plusieurs activités vues précédemment
- Proposer une évaluation conséquente type _Bac blanc_ sur une partie conséquente du programme   
