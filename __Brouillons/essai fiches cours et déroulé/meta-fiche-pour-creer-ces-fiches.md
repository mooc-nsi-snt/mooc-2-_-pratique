# A propos de la création des fiches profs et déroulé des activités

On ira sur le site https://pixees.fr/informatiquelycee sélectionner une fiche activité et à partir du modèle on crée trois documents:
- une copie de la fiche élève d'activité que nous allons faire évoluer
- une fiche prof vide à partir du modèle
- une fiche déroulé vide à partir du modèle

## Mise en place des méta-données

On commence par bien remplir les méta-données, par exemple en regardant quel point du programme est couvert, en mettant le lien vers la fiche de cours correspondante sur le site ou ailleurs selon son choix

## Création du déroulé

On se met à la place des élèves et on "déroule" l'activité pour voir quelles étapes et quelles actions précises

On y note toutes ses idées, par exemple pour faire l'activité de manière plus vivante

Au fur et à mesure on modifie sa version de la fiche élève pour adapter à sa façon de faire, à l'ordonnancement de son cours, à la classe qui sera concernée, etc...

`ETC ... QUE DIRE D ASTICIEUX ICI ??`
