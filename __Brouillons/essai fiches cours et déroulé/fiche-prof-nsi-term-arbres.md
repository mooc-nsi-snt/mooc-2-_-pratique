## Exemple de méta-données de fiche prof pour une activité

**Thématique :** Structure de données arborescentes

**Notions liées :** noeuds, racine, enfants, parent, feuille, taille, profondeur, hauteur, récursion, arbre de recherche

**Résumé de l’activité :** exercices sur papier à propos des arbres

**Objectifs :** se familiariser avec la notion d'arbre et son vocabulaire, comprendre le mécanisme des arbres binaires de recherche

**Auteur :** David Roche <"dav74130" <dav74130@gmail.com>; >

**Durée de l’activité :** 5 minutes (non je déconne, je sais pas)

**Forme de participation :** individuelle en autonomie, sur papier.

**Matériel nécessaire :** feuille et crayon, sans ordinateur.

**Préparation :** aucune.

### Références:

**Fiche élève cours :** https://pixees.fr/informatiquelycee/term/c7c.html

**Fiche élève activité :** https://pixees.fr/informatiquelycee/term/c7a.html

**Déroulé :** ./deroule-nsi-term-arbres.md


