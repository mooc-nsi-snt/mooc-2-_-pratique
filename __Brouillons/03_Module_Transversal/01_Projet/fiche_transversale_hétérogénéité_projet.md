# Fiche transversale

## Gestion de l’hétérogénéité dans les projets

Dans le cadre d’un projet, vous avez proposé à vos élèves de première NSI de réaliser un jeu du pendu en Python en mode console. Certains de vos élèves, très autonomes, ont déjà bien avancé dans ce projet après 1h de travail. En revanche, quelques élèves éprouvent des difficultés à “démarrer”.
Proposer des idées de stratégies que vous pourriez mettre en place afin d’aider ces élèves à “démarrer” le projet.
Après 5h de travail (vous avez prévu de consacrer 6h sur ce projet), vous constatez que certains élèves peinent à terminer le projet. Proposez des idées de stratégies qui permettraient à ces élèves de terminer le projet dans le temps imparti.
