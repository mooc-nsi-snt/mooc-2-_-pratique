# Entretien avec Olivier Goletti 3/5 : quelques exemples.

Olivier Goletti est un jeune chercheur en didactique de l'informatique. Ces entretiens permettent d'éclairer ce que recouvre 
ce nouveau champs disciplinaire.

## Entretien avec Olivier Goletti, Doctorant en didactique de l'informatique à l'UCLouvain

**Sommaire des 5 vidéos**

* 1/5 [Olivier Goletti, qui es-tu ?](./1_Entretien_Olivier_Goletti1_5.md)
* 2/5 [Didactique de l'informatique ?](./1_Entretien_Olivier_Goletti2_5.md ) 
* **3/5 Quelques exemples\.**
* 4/5 [Pensée informatique\.](./1_Entretien_Olivier_Goletti4_5.md)
* 5/5 [Quelques conseils plus pratiques\.](./1_Entretien_Olivier_Goletti5_5.md)


## 3/5 Quelques exemples

Dans cette video, Olivier Goletti éclaire par des exemples concrets la portée des notions de didactique de l'informatique

[![Entretien Olivier Goletti 3/5](https://mooc-nsi-snt.gitlab.io/portail/assets/Diapo-itw-OG1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-OG-3.mp4)

Regardons quelques exemples :

On sait qu'apprendre à programmer est quelque chose de compliqué[1](#ref1). La recherche, jusqu'il y a peu, se faisait principalement au niveau de l'apprentissage universitaire et les études convergent vers ce constat que c'est compliqué. Par exemple, il est difficile d'exécuter mentalement un programme car il est compliqué de retenir dans sa tête l'état des variables de la mémoire du programme. Le "tracing" (ou "tracer") qui consiste à mettre à jour sur papier en marge des lignes de codes d'un programme ce qu'il advient de l'état de la mémoire du programme au fil des instructions, pour justement comprendre l'état de ce programme. 

On a aussi identifié beaucoup de "misconceptions"[2 (surtout l'annexe 1)](#ref2), par exemple le fait qu'une variable retienne les variables précédentes qui ont été affectées, alors que (sauf structure de donnée idoine) affecter une valeur efface celle qui précède, ce qui est du à la manière dont les variables sont présentées : si on présente les variables avec la métaphore d'une boite dans lequel on met quelque chose on va avoir tendance à imaginer pouvoir y mettre plusieurs choses, si on parle de postit que l'on colle et décolle, cela amène une autre image.

Un autre sujet concerne le mythe erroné du "gêne de l'informatique" (comme la bosse des maths), on observe qu'avec le temps il n'y a pas d'obstacle à apprendre à programmer, avec juste plus ou moins de facilité[3](#ref3).

Un autre axe de recherche rapproche la lecture d'un programme, donc d'un langage formel, avec la lecture de la langue humaine, par exemple quand on commence des textes au plus jeune âge, on va répéter des mots, lire à haute voix, et ce qui a été montré est que dans un programme il y a des termes formels et des mots de la langue naturelle, et se mettre d'accord sur la façon de le lire aide à clarifier les notions sous-jacentes[4](#ref4). De même, la manière d'apprendre à écrire peut inspirer l'apprentissage de l'écriture d'un programme, par exemple en amenant les éléments de syntaxe (par exemple les guillemets des chaînes de caractères) au fur et à mesure des besoins, comme l'introduction de la ponctuation et des autres éléments de l'écriture se fait progressivement.

Un dernier exemple est l'apprentissage entre pairs une technique d'instruction qui évaluait les étudiant·e·s par un quiz, puis au vue des résultats faisait en sorte que les étudiant·e·s s'expliquent l'un·e l'autre leurs réponses, faisant le constat que des personnes qui viennent juste de comprendre et sont proches intellectuellement d'autres en apprentissage sont plus à même d'expliquer ce qui manque, plutôt que de redonner le savoir de l'enseignant·e·s. Tirée de la didactique de la physique, cette technique est largement utilisée dans l'enseignement de l'informatique[5](#ref5).

Au delà, on peut analyser les stades de compréhension des concepts en informatique, néo-Piagietiens[6](#ref6) pour dire, comme illustré [sur ce graphe](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Prendre%20du%20recul%20au%20niveau%20didactique/Entretien_avec_un_jeune_didacticien_de_l'Informatique/slides-olivier-goletti.pdf#page=7). Pour chaque nouveau concept on distingue trois stades, en prenant l'exemple de "tracing" de programme évoqué ci dessus, un stade sensori-moteur de pre-tracing où on commence à comprendre des bribes, par exemple de syntaxe, un stade pré-opérationnel où on est en mesure de suivre localement ce qui se passe au fil des instructions et puis une troisième phase où on devient opérationnel par rapport à la notion étudiée. Ce qui est intéressant avec ce modèle, c'est qu'on montre qu'au fur et à mesure qu'on apprend de nouveaux concepts, on risque de régresser sur les concepts précédents, de manière temporaire.

## Références

[1] <a id="ref1"></a> Lister, R., Adams, E. S., Fitzgerald, S., Fone, W., Hamer, J., Lindholm, M., McCartney, R., Moström, J. E., Sanders, K., & Seppälä, O. (2004). _A multi-national study of reading and tracing skills in novice programmers_. ACM SIGCSE Bulletin, 36, 119‑150.

[2] <a id="ref2"></a> Sorva, J. (2012). _Visual program simulation in introductory programming education. Aalto University_. https://aaltodoc.aalto.fi:443/handle/123456789/3534


[3] <a id="ref3"></a> Guzdial, M. (2020). _Is there a Geek Gene? Are CS grades bi-modal? Moving computing ed research forward_. https://computinged.wordpress.com/2020/01/21/is-there-a-geek-gene-are-cs-grades-bi-modal-moving-computing-ed-research-forward/


[4] <a id="ref4"></a> Hermans, F., Swidan, A., & Aivaloglou, E. (2018). _Code phonology : An exploration into the vocalization of code_. Proceedings of the 26th Conference on Program Comprehension  - ICPC ’18, 308‑311. https://doi.org/10.1145/3196321.3196355


[5] <a id="ref5"></a> Porter, L., Bouvier, D., Cutts, Q., Grissom, S., Lee, C., McCartney, R., Zingaro, D., & Simon, B. (2016). _A Multi-institutional Study of Peer Instruction in Introductory Computing_. Proceedings of the 47th ACM Technical Symposium on Computing Science Education, 358‑363. https://doi.org/10.1145/2839509.2844642

[6] <a id="ref6"></a> Lister, R. (2016). _Toward a developmental epistemology of computer programming_. Proceedings of the 11th workshop in primary and secondary computing education, 5‑16.

