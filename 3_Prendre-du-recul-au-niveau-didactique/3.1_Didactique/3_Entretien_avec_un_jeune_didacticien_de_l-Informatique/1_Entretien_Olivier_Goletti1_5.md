# Entretien avec Olivier Goletti 1/5 : de la didactique de l'informatique.

Olivier Goletti est un jeune chercheur en didactique de l'informatique. Ces entretiens permettent d'éclairer ce que recouvre 
ce nouveau champs disciplinaire.

## Entretien avec Olivier Goletti, Doctorant en didactique de l'informatique à l'UCLouvain

**Sommaire des 5 vidéos**

* **1/5 Olivier Goletti, qui es-tu ?** 
* 2/5 [Didactique de l'informatique ?](./1_Entretien_Olivier_Goletti2_5.md ) 
* 3/5 [Quelques exemples\.](./1_Entretien_Olivier_Goletti3_5.md)
* 4/5 [Pensée informatique\.](./1_Entretien_Olivier_Goletti4_5.md)
* 5/5 [Quelques conseils plus pratiques\.](./1_Entretien_Olivier_Goletti5_5.md)


## 1/5 Olivier Goletti, qui es-tu ?

 Présentation d'Olivier Goletti, doctorant en didactique de l'informatique, et des objectifs de ses travaux de recherche.

[![Entretien Olivier Goletti 1/5](https://mooc-nsi-snt.gitlab.io/portail/assets/Diapo-itw-OG1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-OG-1.mp4)

[Olivier Goletti](https://uclouvain.be/olivier.goletti), est assistant chercheur et doctorant en didactique de l'informatique depuis deux ans au pôle informatique, [INGI](https://uclouvain.be/fr/instituts-recherche/icteam/ingi) de l'UCLouvain en collaboration avec le [Programming Education Research Lab](https://perl.liacs.nl/) de l'Université de Leiden. Sa recherche en didactique vise à aider les enseigant·e·s à pouvoir utiliser des techniques d'enseignement de l'informatique qui fonctionnent bien.

Accéder aux [slides de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Prendre%20du%20recul%20au%20niveau%20didactique/Entretien_avec_un_jeune_didacticien_de_l'Informatique/slides-olivier-goletti.pdf).




