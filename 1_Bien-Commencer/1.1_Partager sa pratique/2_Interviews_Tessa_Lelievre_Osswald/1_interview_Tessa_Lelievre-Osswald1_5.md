# Interview Tessa Lelièvre-Osswald 1/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* **1/5 Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours ?** 
* 2/5 [Comment se former et préparer ses cours ?](./1_interview_Tessa_Lelievre-Osswald2_5.md) 
* 3/5 [Pratique en classe\.](./1_interview_Tessa_Lelievre-Osswald3_5.md)
* 4/5 [Enseignement adapté aux différents élèves\.](./1_interview_Tessa_Lelievre-Osswald4_5.md)
* 5/5 [Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)


## 1/5 Tessa Lelièvre-Osswald, qui es tu, quel est ton parcours ? Pourquoi avoir envie d'enseigner ?

Dans cette vidéo Tessa Lelièvre_Osswald, jeune professeure certifiée au tout nouveau CAPES d'informatique, nous raconte comment son projet de devenir enseignante en mathématique a évolué vers celui de devenir enseignante en NSI.

[![Interview Tessa Lelièvre Osswald 1/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-1.mp4)


### Enseigner : une vocation.


_Bonjour Tessa! Qui es tu et quel est ton parcours ?_

Bonjour, je suis Tessa Lelièvre-Osswald. J'ai 24 ans. Je suis en région parisienne, pas loin de Versailles, et j'enseigne la NSI. Quant à mon parcours, moi, j'ai toujours voulu être prof de maths et depuis petite puisque j'adore transmettre. Et du coup, j'ai eu mon bac S spécialité maths. Suite à quoi j'ai commencé une licence de maths. Mais au cours de cette licence, on avait des cours d'informatique et ça me plaisait plus que les maths. Donc je me suis réorientée. J'ai fait une licence d'informatique à l'Université Diderot, qui s'appelle maintenant Université de Paris. Suite à quoi, j'ai fait une première année de master recherche, de même à l'Université Diderot, puis à l'[MPRI](https://wikimpri.dptinfo.ens-cachan.fr/doku.php), donc pareil à Diderot, mais qui est en partenariat avec l'[ENS](https://www.ens.psl.eu) et l'[Ecole polytechnique](polytechnique.edu). À la fin de cette année de master, j'ai commencé un stage à l'[IRIF](https://www.irif.fr), en recherche , en combinatoire. Et pendant ce temps, j'ai aussi passé le [CAPES](https://mooc-nsi-snt.gitlab.io/portail/4_Apprendre_A_Enseigner/Le_Mooc/4_Que-faire-pour%20se-preparer-aux-concours/0_introduction.html), qui venait d'être créé pour être prof NSI.
  
_Tu disais ton plaisir de transmettre. Alors pourquoi avoir envie d'enseigner ?_

En fait, déjà, je me souviens quand j'étais au collège et que j'avais des profs de math que moi j'appréciais, mais que mes camarades avaient du mal à suivre, et j'ai l'impression que beaucoup d'élèves n'aiment pas les maths parce que peut être qu'ils n'avaient pas le bon lien avec leurs enseignants et que c'était vraiment au moment du collège que ça arrivait. Donc je pensais que c'était super important d'avoir des profs qui ont envie de transmettre et d'utiliser différentes méthodes pour éviter justement ce phénomène de d'élève qui abandonne les maths, en l'occurrence très tôt. Donc, c'est pour ça que, de base, moi, je voulais être prof de math au collège, donc à la fois parce que j'adorais, et pour éviter le décrochage puisque c'est une matière très intéressante. Et c'est dommage de s'arrêter juste parce qu'on a du mal avec notre enseignant. Et bon, il se trouve que finalement, c'est de l'informatique, mais ça s'applique de la même manière, je pense.


