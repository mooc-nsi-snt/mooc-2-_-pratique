# Pourquoi utiliser l'ouvrage Enseigner l'informatique, Hartmann et al.?

Si la didactique de l'Informatique est une discipline jeune, elle n'est toutefois pas vierge de jalons, de repères, que d'autres ont posés. Dans ce Mooc, nous vous proposons de (re)découvrir un ouvrage : "**Enseigner l'Informatique**" par _Werner Hartmann, Michael Näf et Raimond Reichert_. De façon très pragmatique les auteurs présentent et illustrent ce qu'est enseignement d'Informatique, ce qu'il n'est pas et ce qu'il ne devrait surtout pas devenir. Leur propos est soutenu par des exemples concrets et avisés.

**Cet ouvrage de référence à structuré notre Mooc et comme un fil rouge, nous vous proposons dans chaque modules les chapitres de cet ouvrage qui lui correspondent.**
