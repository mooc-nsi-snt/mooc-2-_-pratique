# Le sac de bonbons

- **Fiche élève cours :** https://pixees.fr/informatiquelycee/n_site/nsi_term_structDo_graphe.html
- **Fiche élève activité :** Il n'y en a pas car l'activité se découvre au fil de l'eau (c'est le professeur qui suit son _déroulé_).
- **Déroulé :** _si déjà connu_

--- 

**Thématique :** Structures hiérarchiques

**Notions liées :** arbres, arbres binaires, graphes.

**Résumé de l’activité :** modéliser et implémenter un calcul de plus court chemin sur un problème concret

**Objectifs :** comprendre la structure de graphe, modéliser une situation par un graphe, vérifier un algorithme

**Auteur :** Thierry Viéville <thierry.vieville@inria.fr>

**Durée de l’activité :** 2H environ.

**Forme de participation :** travail préparatoire en binôme, réalisation individuelle en autonomie, validation en groupe.

**Matériel nécessaire :** un sac d'une dizaine de bonbons (chaque bonbon est emballé car on va les manipuler, on pourra aussi prendre un sac pour que chaque élève ait un bonbon à la fin), une dizaine de postits, ordinateurs avec installé un interprèteur Python.

**Préparation :** aucune

**Autres Références :** 

- [programme NSI terminale](https://cache.media.education.gouv.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf#page=4)
- [Cours sur les arbres](https://pixees.fr/informatiquelycee/n_site/nsi_term_structDo_graphe.html)
- [Une activité débranchée sur les arbres](https://pixees.fr/wp-content/uploads/2014/08/la-ville-embourbee.pdf)

